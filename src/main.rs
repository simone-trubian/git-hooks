#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate walkdir;

use walkdir::{DirEntry, WalkDir};
use std::path::{PathBuf, Path};
use std::fs::File;
use std::env;
use std::process::exit;
use std::io::{BufReader, BufRead, Error, Read};

struct Config {
    extensions: Vec<String>,
    disallowed_words: Vec<String>,
    current_dir: PathBuf,
    source_files: Vec<DirEntry>,
}

#[derive(Deserialize)]
struct FileConfig {
    extensions: Vec<String>,
    disallowed_words: Vec<String>,
}

impl Config {
    fn find_files(&mut self) -> () {
        self.source_files = WalkDir::new(&self.current_dir)
            .into_iter()
            .filter_entry(|e| !Config::is_hidden(e))
            .filter_map(Result::ok)
            .filter(|e| self.is_allowed(e))
            .collect();
    }

    fn is_hidden(entry: &DirEntry) -> bool {
        entry.file_name()
            .to_str()
            .map(|s| s.starts_with("."))
            .unwrap_or(false)
    }

    fn is_allowed(&self, entry: &DirEntry) -> bool {
        match entry.file_name().to_str() {
            Some(entry_name) => self.map_name(entry_name),
            None => false,
        }
    }

    fn map_name(&self, name: &str) -> bool {
        let extensions = &self.extensions;
        extensions.into_iter().any(|ex| name.ends_with(ex))
    }

    fn find_comments(&self) -> Result<bool, Error> {
        let files = &self.source_files;
        let current_file = File::open(files[0].path())?;
        let dis1: &str = &*self.disallowed_words[0]; // String de-referencing
        Ok(BufReader::new(current_file).lines().any(|line| line.unwrap().contains(dis1)))
    }
}

fn main() {

    let mut config = new_config("hook.toml");
    config.find_files();

    match config.find_comments() {
        Ok(false) => {println!("found nothing"); exit(0)},
        Ok(true) => {println!("found stuff"); exit(1)},
        Err(_) => println!("Error!")
    }
}

fn new_config<P>(filename: P) -> Config
where
    P: AsRef<Path>,
{
    let conf_content: &mut String = &mut String::from("");
    let file = File::open(filename).expect("Hook configuration file not found");
    BufReader::new(file).read_to_string(conf_content).expect("Error");
    let file_config: FileConfig = toml::from_str(conf_content).expect("Error");

    return Config{
        extensions: file_config.extensions,
        disallowed_words: file_config.disallowed_words,
        current_dir: env::current_dir().unwrap(),
        source_files: Vec::new()
    }
}
